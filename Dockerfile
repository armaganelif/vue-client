#FROM node:latest
#RUN npm install -g http-server
#WORKDIR ./dist/spa
#
#EXPOSE 8095
#CMD [ "http-server", "dist/spa/index.html" ]
#

FROM nginx:alpine
COPY ./dist/spa /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
